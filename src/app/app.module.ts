import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";

import { AppComponent } from './app.component';
import { FotoModule } from "./foto/foto.module";
import { CardModule } from "./card/card.module";
import { ListagemComponent } from './listagem/listagem.component';
import { CadastroComponent } from './cadastro/cadastro.component';
import { roteamento } from "./app.routes";
import { FotoService } from './services/foto.service';
import { RouterModule } from '@angular/router';
import { MaterializeModule } from "angular2-materialize";
import { FiltroPorTitulo } from './listagem/listagem.pipe';
import { FotoComponent } from './foto/foto.component';

@NgModule({
  declarations: [
    AppComponent,
    ListagemComponent,
    CadastroComponent, FiltroPorTitulo
  ],
  imports: [
    MaterializeModule,
    RouterModule,
    BrowserModule,
    HttpClientModule,
    FotoModule,
    CardModule,
    FormsModule,
    roteamento
  ],
  providers: [FotoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
