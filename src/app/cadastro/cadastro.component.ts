import { Component, OnInit } from '@angular/core';
import { FotoComponent } from '../foto/foto.component';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { FotoService } from '../services/foto.service';
import { Observable } from 'rxjs/Observable';

@Component({
    selector: 'cadastro',
    templateUrl: './cadastro.component.html',
    styles: []
})
export class CadastroComponent implements OnInit {
    id
    foto = new FotoComponent()
    message


    constructor(private fotoService: FotoService, private activatedRouter: ActivatedRoute, private router: Router) {
        activatedRouter.params.subscribe(
            params => {
                if(params.id != null){
                    console.log(params)
                    fotoService.findOne(params.id).subscribe(
                        fotoBD => this.foto = fotoBD
                    )
                }
            })
     }

    ngOnInit() {
        
     }

    salvar() {

        console.log(this.foto)
        var observable: Observable<Object>
        if(this.foto._id != ''){
            observable = this.fotoService.update(this.foto)
        } else {
            observable = this.fotoService.save(this.foto)
        }

        observable.subscribe(
            response => {
                console.log(response);
                this.message = response
                setTimeout(() => {
                    this.router.navigate([''])
                }, 2000);
                
            },
            error => console.log(error)
        )

    }
}