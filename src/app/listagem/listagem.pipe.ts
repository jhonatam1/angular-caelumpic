import { Pipe, PipeTransform } from "@angular/core";
import { FotoComponent } from "../foto/foto.component";

@Pipe({
    name: 'filtroPorTitulo'
})
export class FiltroPorTitulo implements PipeTransform{

    transform(fotos: FotoComponent[], digitado: string): FotoComponent[]{
        if (!digitado) return fotos;
        digitado = digitado.toLowerCase()
        return fotos.filter( foto => foto.titulo.toLowerCase().includes(digitado))
    }
}