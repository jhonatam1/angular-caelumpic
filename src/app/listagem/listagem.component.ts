import { Component, OnInit } from '@angular/core';

import { FotoService } from "../services/foto.service";
import { FotoComponent } from '../foto/foto.component';

@Component({
  selector: 'app-listagem',
  templateUrl: './listagem.component.html',
  styles: []
})
export class ListagemComponent implements OnInit {
  message
  listaFotos: FotoComponent[]

  constructor(private fotoService: FotoService){      

    this.fotoService.listAll()
        .subscribe(
          (fotosApi: FotoComponent[]) => this.listaFotos = fotosApi
          , erro => console.log(erro)                    
        )
  }

  fotosChange(event) {
    console.log(event, 'teste');
    this.listaFotos = event.listaFotos;
    this.message = event.message
    
    setTimeout(() => {this.message = ''}, 2000);
  }

  ngOnInit() {}

}
