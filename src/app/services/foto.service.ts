import { HttpClient, HttpHeaders } from "@angular/common/http";

import { FotoComponent } from "../foto/foto.component";
import { Injectable } from "@angular/core";

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'

const url = 'http://localhost:3000/v1/fotos/'
const header = {
   headers: new HttpHeaders({'Content-Type': 'application/json'})
}

@Injectable()
export class FotoService{
    
    constructor(private serverApi: HttpClient){}
    
    listAll(){
        return this.serverApi.get<FotoComponent[]>(url)
    }
    
    findOne(id: String){
        return this.serverApi.get<FotoComponent>(url+id)
    }

    save(foto: FotoComponent){
        return this.serverApi.post(url,foto,header)
            .map(
                () => (`${foto.titulo} incluída com sucesso!`)
            )
    }

    delete(id: String){
        return this.serverApi.delete(url+id)
            .map(
                () => (`Foto excluída com sucesso!`)
            )
    }


    update(foto: FotoComponent){
        return this.serverApi.put(url+foto._id,foto,header)
            .map(
                () => (`${foto.titulo} alterada com sucesso!`)
            )
    }
}