import { Component, Input, Output, EventEmitter } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Router } from "@angular/router";

import { FotoService } from "../services/foto.service";
import { FotoComponent } from "../foto/foto.component";
import { MaterializeDirective } from "angular2-materialize";

@Component({
    selector: 'card',
    templateUrl: './card.component.html'
})
export class CardComponent {
    @Input() foto: FotoComponent
    @Input() fotos: FotoComponent[]

    @Output() updatedFotos = new EventEmitter();

    constructor(private fotoService: FotoService, private router: Router) { }

    ngOnInit() { }

    deletar() {

        console.log(this.foto._id)

        this.fotoService.delete(this.foto._id).subscribe(
            response => {
                console.log(response, 'Ok!');
                this.fotos = this.fotos.filter(foto => foto._id != this.foto._id)
                this.updatedFotos.emit({listaFotos: this.fotos, message: response});
            },
            error => {console.log(error, 'não Ok!');}
        )
    }
}